package com.example.examen1

import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import java.util.concurrent.ThreadLocalRandom

class MenuFragment : Fragment() {
    // TODO: Rename and change types of parameters

    private var listener: OnFragmentInteractionListener? = null

    internal lateinit var suma: Button
    internal lateinit var resta: Button
    internal lateinit var multiplicacion: Button
    internal lateinit var division: Button
    //internal lateinit var playerName: TextView

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        //playerName = view.findViewById(R.id.player_name)
        suma = view.findViewById(R.id.sumaboton)
        suma.setOnClickListener { goToSuma() }
        resta = view.findViewById(R.id.restaboton)
        resta.setOnClickListener { goToResta() }
        multiplicacion = view.findViewById(R.id.multiplicacionboton)
        multiplicacion.setOnClickListener { goToMultiplicacion() }
        division = view.findViewById(R.id.divisionboton)
        division.setOnClickListener { goToDivision() }

    }
////////////
    fun goToSuma() {//suma

        val action = MenuFragmentDirections
            .actionMainGame("suma",""+generarAzar(20),""+generarAzar(20))
        view?.findNavController()?.navigate(action)
    }

    fun goToResta() {
        val action = MenuFragmentDirections
            .actionMainGame("resta",""+generarAzar(20),""+generarAzar(20))
        view?.findNavController()?.navigate(action)
    }

    fun goToMultiplicacion() {
        val action = MenuFragmentDirections
            .actionMainGame("multiplicacion",""+generarAzar(20),""+generarAzar(20))
        view?.findNavController()?.navigate(action)
    }

    fun goToDivision() {
        val action = MenuFragmentDirections
            .actionMainGame("division",""+generarAzar(20),""+generarAzar(20))
        view?.findNavController()?.navigate(action)
    }

    /////////////
    fun sumar(n1:Int, n2:Int):Int{
        return n1+n2
    }

    fun restar(n1:Int, n2:Int):Int{
        return n1-n2
    }

    fun multiplicar(n1:Int, n2:Int):Int{
        return n1*n2
    }

    fun dividir(n1:Int, n2:Int):Double{
        return n1.div(n2).toDouble()
    }



    fun generarAzar(range: Int): Int {
        return ThreadLocalRandom.current().nextInt(range)
    }


    //////////


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_menu, container, false)
    }



    // TODO: Rename method, update argument and hook method into UI event
    fun onButtonPressed(uri: Uri) {
        listener?.onFragmentInteraction(uri)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException(context.toString() + " must implement OnFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        fun onFragmentInteraction(uri: Uri)
    }
}

